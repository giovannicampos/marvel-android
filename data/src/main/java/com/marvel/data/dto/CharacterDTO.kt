package com.marvel.data.dto

import com.marvel.data.models.Data

class CharacterDTO(
    val data: Data? = null
)