package com.marvel.data.contracts

import com.marvel.data.contracts.abstracts.Repository
import com.marvel.data.dto.CharacterDTO

interface CharacterRepository : Repository<CharacterDTO>