package com.marvel.data.utils

fun getCurrentTimeStamp(): String =
    (System.currentTimeMillis() / 1000).toString()
